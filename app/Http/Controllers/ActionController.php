<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 20.03.2018
 * Time: 15:35
 */

namespace App\Http\Controllers;

use App\Action;
use Illuminate\Http\Request;
use DateTime;


class ActionController extends Controller
{
    public function getIndex(){

        $actions = Action::all();
        return view('index', ['actions' => $actions]);
    }

    public function saveAction(Request $request){

        $actions = simplexml_load_file($request->file('action')->getRealPath());

        foreach($actions as $action){

            $action_obj = new Action;
            $action_obj->title = $action->title;
            $action_obj->description = $action->description;

            //Поиск совпадений по заглавиям и описаниям акций и запись даты обновления акции
            $search_id = Action::select('id')->where('title', $action->title)->where('description', $action->description)->first();

            if(!empty($search_id)){
                $action_obj->where('id', $search_id->id)->update([
                    'url' => $action->url,
                    'image' => $action->image,
                    'date_start' => $action->date_start,
                    'date_end' => $action->date_end,
                    'products_num' => count($action->products->product),
                    'updated' => date("Y-m-d")
                ]);

                //Если нет совпадений названия акции и описания, просто берем данные из пришедшего xml, а updated поле у нас 1111,
                // главное чтоб меньше 2000 года
            } else {
                $action_obj->url = $action->url;
                $action_obj->image = $action->image;
                $action_obj->date_start = $action->date_start;
                $action_obj->date_end = $action->date_end;
                $action_obj->products_num = count($action->products->product);
                $action_obj->updated = '1111-11-11';
                $action_obj->save();
            }
        }

        return redirect()->route('index');
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function validateAction(Request $request){

        try{
            simplexml_load_file($request->file('action')->getRealPath());
        }
        catch(\Exception $e){
            return redirect()->route('index')->with(['error' => "Ошибка! Неподдерживаемая кодировка, формат файла или 
            отсутствует ветка <sales>"]);
        }

        $actions = simplexml_load_file($request->file('action')->getRealPath());


        $i = 1; //Установим счетчик
        foreach($actions as $key => $action){

            if($key != "sale"){
                return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Отсутствует или некорректен необходимый элемент sale в элементе '.$i]);
            }

            if(!$action->title || strlen($action->title) > 150 || strlen($action->title) < 1){
                return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Отсутствует или слишком длинный заголовок в элементе '.$i.'!']);
            }

            if(strlen($action->description) > 500){
                return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Слишком длинное описание в элементе '.$i]);
            }

            if(!$action->date_start || $this->validateDate($action->date_start) != true){
                return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Отсутствует или не соотвествтует формату дата начала акции в элементе '.$i]);
            }

            if(!$action->date_end|| $this->validateDate($action->date_end) != true){
                return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Отсутствует или не соотвествтует формату дата окончания акции в элементе '.$i]);
            }

            if(!$action->products){
                return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Отсутствует список продуктов, участвующих в акции. Ошибка в элементе '.$i]);
            } else foreach($action->products->children() as $product){
                if(strlen($product['id']) > 20){
                    return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                        'Слишком длинный айди товара в элементе '.$i]);
                }

                if(isset(get_object_vars($product)[0])){
                    if(!filter_var(get_object_vars($product)[0], FILTER_VALIDATE_URL)){
                        return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                            'Cсылка на продукт в элементе ' . $i.' не является url']);
                    }
                } else  return redirect()->route('index', ['actions' => $actions])->with(['error' =>
                    'Отсутствует ссылка на продукт в элементе ' . $i]);
            }
            $i++;
        }

        return redirect()->route('index', ['actions' => $actions])->with(['success' => 'Акции успешно провалидированы!']);
    }

    public function deleteAction($action_id){

        $action = Action::find($action_id);

        $action->delete();

        return redirect()->route('index')->with(['success' => 'Акция успешно удалена!']);
    }
}