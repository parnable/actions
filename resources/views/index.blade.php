@extends('layouts.master')

@section('title')
    Актуальные акции
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
    @if(Session::has('error'))
        <section class="info-box fail">
            {{ Session::get('error') }}
        </section>
    @endif
    @if(Session::has('success'))
        <section class="info-box success">
            {{ Session::get('success') }}
        </section>
    @endif
    @if($actions)
    <section class="actions">
        <h1>Список акций</h1>
        @for($i = 0; $i < count($actions); $i++)
            <article class="action">
                <div class="delete"><a href="{{ route('delete', ['action_id' => $actions[$i]->id]) }}">x</a></div>
                <div class="title">{{ $actions[$i]->title }}</div>
                {{ $actions[$i]->description }}
                <div class="products_num">В акции участвует {{ $actions[$i]->products_num }} товаров</div>
                <div class="period">Период действия акции с {{ $actions[$i]->date_start }} по {{ $actions[$i]->date_end }}</div>
                @if(round($actions[$i]->updated) > 2000 )
                <div class="info">Акция обновлена {{ $actions[$i]->updated }}</div>
                @endif
            </article>
        @endfor
    </section>
    @endif
    <section class="edit-action">
        <h2>Добавить акции</h2>
        <form method="post" enctype="multipart/form-data" action="{{ route('save') }}">
            <div class="input-group">
                <label for="action_upload">Загрузите файл с акциями</label>
                <input type="file" name="action" id="action_upload" />
            </div>
            <button type="submit" class="btn">Сохранить акции</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}" />
        </form>
        <h2>Валидировать акции</h2>
        <form method="post" enctype="multipart/form-data" action="{{ route('validate') }}">
            <div class="input-group">
                <label for="action_upload">Загрузите файл с акциями</label>
                <input type="file" name="action" id="action_upload" />
            </div>
            <button type="submit" class="btn">Валидировать!</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}" />
        </form>
    </section>
@endsection