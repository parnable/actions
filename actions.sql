-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 03 2018 г., 18:07
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `actions`
--

-- --------------------------------------------------------

--
-- Структура таблицы `actions`
--

CREATE TABLE `actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `products_num` int(11) NOT NULL,
  `updated` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `actions`
--

INSERT INTO `actions` (`id`, `title`, `description`, `url`, `image`, `date_start`, `date_end`, `products_num`, `updated`, `created_at`, `updated_at`) VALUES
(2, 'При покупке телефона - чехол в подарок', 'При покупке телефона iPhone 5S 16Gb вы получаете в подарок чехол одного из четырех доступных цветов.', 'http://site.com/promotions/12631/>', 'http://site.com.ua/i/promotions/12631.jpg', '2016-11-01', '2017-01-31', 2, '2018-03-20', '2018-03-20 16:41:05', '2018-03-20 16:41:05'),
(24, 'При покупке машин - картуз в подарок', 'При покупке машины Mercedes Benz получаете в подарок чехол одного из четырех доступных форм и оттенков.', 'http://site.com/promotions/12637467/>', 'http://site.com.ua/i/promotions/126jhdf31.jpg', '2016-11-01', '2017-01-31', 2, '2018-03-21', '2018-03-21 07:14:10', '2018-03-21 07:22:58'),
(32, 'Акции в Метро!', 'При проезде более 1000 раз - билетик на один раз в подарок!.', 'http://site.com/promotions/12631/>', 'http://site.com.ua/i/promotions/12631.jpg', '2016-11-01', '2017-01-11', 8, '2018-03-22', '2018-03-21 07:27:03', '2018-03-22 08:40:32'),
(35, 'Заходите к нам!', 'Наши покупательны всегда довольны!', 'http://site.com/promotions/12637467/>', 'http://site.com.ua/i/promotions/126jhdf31.jpg', '2016-11-01', '2017-01-31', 4, '1111-11-11', '2018-03-21 08:36:22', '2018-03-21 08:36:22'),
(36, 'При подписке - призы', 'При подписке на журнал \"Приветик\" вы получаете в подарок чехол одного из четырех доступных планво.', 'http://site.com/promotions/12631/>', 'http://site.com.ua/i/promotions/12631.jpg', '2018-11-01', '2019-01-31', 7, '2018-03-22', '2018-03-21 08:36:22', '2018-03-22 08:40:32'),
(37, 'Заходите к нам!', 'Наши покупательны всегда довольны! волыролврфлоывр лфоырвло фырлвор Наши покупательны всегда довольны! волыролврфлоывр лфоырвло фырлвор Наши покупательны всегда довольны! волыролврфлоывр лфоырвло ', 'http://site.com/promotions/12637467/>', 'http://site.com.ua/i/promotions/126jhdf31.jpg', '2016-11-23', '2017-01-31', 4, '1111-11-11', '2018-03-22 08:40:32', '2018-03-22 08:40:32');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
